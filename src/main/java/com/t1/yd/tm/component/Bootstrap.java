package com.t1.yd.tm.component;

import com.t1.yd.tm.api.controller.ICommandController;
import com.t1.yd.tm.api.controller.IProjectController;
import com.t1.yd.tm.api.controller.IProjectTaskController;
import com.t1.yd.tm.api.controller.ITaskController;
import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;
import com.t1.yd.tm.controller.CommandController;
import com.t1.yd.tm.controller.ProjectController;
import com.t1.yd.tm.controller.ProjectTaskController;
import com.t1.yd.tm.controller.TaskController;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.CommandRepository;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.service.*;
import com.t1.yd.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    public void run(String[] args) {
        initDemoData();
        initLogger();

        processArguments(args);

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void initDemoData() {
        projectRepository.add(new Project("project1", "my first project"));
        projectRepository.add(new Project("project2", "my 2nd project"));
        projectRepository.add(new Project("project3", "my 3rd project"));
        projectRepository.add(new Project("project4", "my 4th project"));
        projectRepository.add(new Project("project5", "my 5th project"));

        taskRepository.add(new Task("task1", "my 1st task"));
        taskRepository.add(new Task("task2", "my 2nd task"));
        taskRepository.add(new Task("task3", "my 3rd task"));
        taskRepository.add(new Task("task4", "my 4th task"));

        projectTaskService.bindTaskToProject(taskRepository.findOneByIndex(0).getId(), projectRepository.findOneByIndex(0).getId());
        projectTaskService.bindTaskToProject(taskRepository.findOneByIndex(1).getId(), projectRepository.findOneByIndex(0).getId());
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
    }

    private void processArgument(final String arg) {
        switch (arg) {
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showInfo();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArgumentError();
        }
    }

    private void processCommand(final String arg) {
        switch (arg) {
            case CommandConstant.ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            case CommandConstant.INFO:
                commandController.showInfo();
                break;
            case CommandConstant.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.PROJECT_GET_BY_ID:
                projectController.showById();
                break;
            case CommandConstant.PROJECT_GET_BY_INDEX:
                projectController.showByIndex();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case CommandConstant.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case CommandConstant.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeById();
                break;
            case CommandConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeByIndex();
                break;
            case CommandConstant.PROJECT_UPDATE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case CommandConstant.PROJECT_UPDATE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case CommandConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConstant.TASK_GET_BY_ID:
                taskController.showById();
                break;
            case CommandConstant.TASK_GET_BY_INDEX:
                taskController.showByIndex();
                break;
            case CommandConstant.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case CommandConstant.TASK_START_BY_ID:
                taskController.startById();
                break;
            case CommandConstant.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case CommandConstant.TASK_COMPLETE_BY_ID:
                taskController.completeById();
                break;
            case CommandConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeByIndex();
                break;
            case CommandConstant.TASK_UPDATE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case CommandConstant.TASK_UPDATE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case CommandConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case CommandConstant.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConstant.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConstant.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case CommandConstant.TASK_SHOW_BY_PROJECT:
                taskController.showByProjectId();
                break;
            case CommandConstant.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void exit() {
        System.exit(0);
    }

}
