package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.controller.IProjectTaskController;
import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.util.TerminalUtil;

public final class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();

        projectTaskService.bindTaskToProject(taskId, projectId);
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();

        projectTaskService.unbindTaskFromProject(taskId, projectId);
    }

}
